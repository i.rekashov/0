import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import wysiwyg from 'vue-wysiwyg'

import App from './components/App.vue'
import students from './components/Students.vue'
import studentinfo from './components/StudentInfo.vue'
import store from './store.js'
import modal from './components/modal.vue'
import login from './components/login.vue'

Vue.use(wysiwyg, {});

const routes = [
   { path: '/', component: students, meta: { requiresAuth: true } },
   { path: '/students-info/:id', component: studentinfo, props: true, meta: { requiresAuth: true } },
   { path: '/login', component: login },
]
const router = new VueRouter({
   routes
})

router.beforeEach((to, from, next) => {
   if (to.matched.some(record => record.meta.requiresAuth)) {
      if (store.getters.getUser === null) {
         next({
            path: '/login',
            query: { redirect: to.fullPath }
         })
      } else {
         next()
      }
   } else {
      next()
   }
})

Vue.use(VueRouter)
Vue.use(VueAxios, axios)
Vue.use(modal)
Vue.use(wysiwyg, {})

new Vue({
   render: h => h(App),
   el: '#app',
   router,
   store
})


