import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        count: 0,
        user: null,
        currentstyle: '',
        user: null
    },
    mutations: {
        setCount: (state, count) => state.count = count,
        setUser: (state, user) => state.user = user,
        setCurrentstyle: (state, currentstyle) => state.currentstyle = currentstyle,
    },
    getters: {
        getCount: (state) => {
            return state.count
        },
        getCurrentstyle: (state) => {
            return state.currentstyle
        },
        getUser: (state) => {
            return state.user
        }
    }
})
export default store;